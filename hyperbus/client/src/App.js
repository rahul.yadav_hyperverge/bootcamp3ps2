import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { HomePage, BookingPage, TicketStatusPage, AdminPage } from './index';
import Menu from './components/atom/menu';
import useApp from './components/logic/useApp';
import AlertDismissible from './components/atom/alert';

function App() {
	const {
		isAdmin,
		setTokenState,
		alertMessage,
		setAlertMessage,
		variant,
		setVariant,
	} = useApp();
	return (
		<div className="app">
			<AlertDismissible variant={variant} message={alertMessage} />
			<div className="app-box">
				<Router>
					<Menu isAdmin={isAdmin} />
					<Switch>
						<Route
							path="/"
							exact
							component={() => (
								<HomePage
									isAdmin={isAdmin}
									setAlertMessage={setAlertMessage}
									setVariant={setVariant}
								/>
							)}
						/>
						<Route
							path="/booking/:id"
							exact
							component={() => (
								<BookingPage 
									isAdmin={isAdmin}
									setAlertMessage={setAlertMessage}
									setVariant={setVariant} 
								/>
							)}
						/>
						<Route
							path="/status"
							exact
							component={() => (
								<TicketStatusPage
									setAlertMessage={setAlertMessage}
									setVariant={setVariant}
								/>
							)}
						/>
						<Route
							path="/admin"
							exact
							component={() => (
								<AdminPage
									isAdmin={isAdmin}
									setTokenState={setTokenState}
									setAlertMessage={setAlertMessage}
									setVariant={setVariant}
								/>
							)}
						/>
					</Switch>
				</Router>
			</div>
		</div>
	);
}
export default App;
