import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';

import Button from './components/atom/button';
import Input from './components/atom/input';

let container = null;
beforeEach(() => {
	container = document.createElement('div');
	document.body.appendChild(container);
});

afterEach(() => {
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

it('render the button with the text', () => {
	act(() => {
		render(<Button text="Submit" />, container);
	});
	expect(container.textContent).toBe('Submit');
});
it('render the Inputs', () => {
	act(() => {
		render(<Input placeholder="First Name" />, container);
	});
	const input = container.querySelector('input');
	expect(input.placeholder).toBe('First Name');
});
