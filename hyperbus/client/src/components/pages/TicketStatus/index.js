import '../../../assests/styles/ticketPage.css';
import '../../../assests/styles/homePage.css';
import { BiSearchAlt } from 'react-icons/bi';
import UseTicket from '../../logic/useTicket';
import Ticket from '../../atom/ticket';
import loadingGif from '../../../assests/gif/eldoret-express.gif';

function TicketStatusPage({ setAlertMessage, setVariant }) {
	const {
		TicketData,
		HandleSearch,
		SearchAnimation,
		UseTicketNumber,
		TicketCardAnimation,
	} = UseTicket({ setAlertMessage, setVariant });

	return (
		<div className="TicketStatus content">
			<div className="search" style={{ animation: SearchAnimation }}>
				<div className="search-items">
					<div className="search-items-box">
						<div className="search-item-icon">
							<BiSearchAlt />
						</div>
						<div className="search-item-input">
							<input
								type="text"
								placeholder="Ticket No."
								onChange={(e) => UseTicketNumber(e.target.value)}
							></input>
						</div>
					</div>
				</div>
				<div className="search-items search-btn" onClick={HandleSearch}>
					Search
				</div>
			</div>
			<div id="loadings-bus-gif">
				<img src={loadingGif}/>
			</div>
			<div className="bus-cards" style={{ animation: TicketCardAnimation }}>
				<Ticket data={TicketData} />
			</div>
		</div>
	);
}
export default TicketStatusPage;
