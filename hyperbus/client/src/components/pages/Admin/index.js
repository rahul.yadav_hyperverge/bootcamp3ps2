import '../../../assests/styles/adminPage.css';
import Input from '../../atom/input';
import Button from '../../atom/button';
import UseAdmin from '../../logic/useAdmin';
import AddBusForm from '../../organisms/addBusForm';
function Admin({ isAdmin, setTokenState, setAlertMessage, setVariant }) {
	const { HandleLogIn, HandleEmail, HandlePassword, HandleLogout } = UseAdmin({
		setTokenState,
		setAlertMessage,
		setVariant,
	});
	if (!isAdmin) {
		return (
			<div className="admin-login">
				<div className="admin-login-box">
					<div className="heading-text">Log In</div>
					<div className="admin-form">
						<Input width="100%" placeholder="Email" onChange={HandleEmail} />
						<Input
							width="100%"
							placeholder="Password"
							type="password"
							onChange={HandlePassword}
						/>
						<Button text="Log In" width="95%" onClick={HandleLogIn} />
					</div>
				</div>
			</div>
		);
	}
	return (
		<div className="admin">
			<div className="admin-panel">
				<div className="admin-logout" onClick={HandleLogout}>
					Log Out
				</div>
			</div>
			<div className="add-new-bus-form">
				<AddBusForm
					setAlertMessage={setAlertMessage}
					setVariant={setVariant}
				/>
			</div>
		</div>
	);
}
export default Admin;
