import '../../../assests/styles/bookingPage.css';
import moment from 'moment';
import { RiSteering2Fill } from 'react-icons/ri';
import Row from '../../atom/row';
import Form from '../../organisms/form';
import UseBooking from '../../logic/useBooking';
import UserDisplayForm from '../../organisms/userDisplayForm';
import { useParams } from "react-router-dom";
function BookingPage({ isAdmin,setAlertMessage,setVariant }) {
	const { id } = useParams();
	const {
		seat,
		busData,
		seatNumbers,
		handleAge,
		handleSeat,
		handleFirstName,
		handleLastName,
		handlePhoneNumber,
		handleSubmit,
		userFirstName,
		userLastName,
		userPhoneNumber,
		userAge,
	} = UseBooking({ busId: id, setAlertMessage, setVariant});
	let formDisplay;
	if (!isAdmin) {
		formDisplay = (
			<Form
				seat={seat}
				handleSubmit={handleSubmit}
				handleAge={handleAge}
				handleFirstName={handleFirstName}
				handleLastName={handleLastName}
				handlePhoneNumber={handlePhoneNumber}
			/>
		);
	} else {
		formDisplay = (
			<UserDisplayForm
				userSeat={seat}
				userFirstName={userFirstName}
				userLastName={userLastName}
				userPhoneNumber={userPhoneNumber}
				userAge={userAge}
			/>
		);
	}
	const formatedDate = moment(busData.departureDate).format('DD-MM-YYYY');
	return (
		<div className="Booking content">
			<div className="booking-box">
				<div className="booking-bus-schedule">
					<div className="booking-date-id-box">
						<div className="booking-bus-date">Date : {formatedDate}</div>
						<div className="booking-bus-id">Bus No : {busData._id}</div>
					</div>
					<div className="booking-bus-time">
						<div className="booking-bus-departure-time">
							<span>{busData.departureTiming}</span>
						</div>
						<div className="booking-bus-arrival-time">
							<span>{busData.arrivalTiming}</span>
						</div>
					</div>
					<div className="booking-bus-location">
						<div className="booking-bus-departure-location">
							<span>{busData.departure}</span>
						</div>
						<div className="booking-bus-arrival-location">
							<span>{busData.arrival}</span>
						</div>
					</div>
					<div className="booking-bus-time-text">
						<div className="booking-bus-departure-text">From</div>
						<div className="booking-bus-arrival-text">To</div>
					</div>
				</div>
				<div className="booking-bus-seats">
					<div className="booking-bus-seats-box">
						<div className="bus-steering-logo">
							<RiSteering2Fill />
						</div>
						{seatNumbers.map((seatNumber) => (
							<Row
								key={seatNumber[0]}
								seatNumber={seatNumber}
								handleSeat={handleSeat}
							/>
						))}
					</div>
				</div>
				<div className="booking-form">
					<div className="booking-form-box">{formDisplay}</div>
				</div>
			</div>
		</div>
	);
}
export default BookingPage;
