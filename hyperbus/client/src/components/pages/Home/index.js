import '../../../assests/styles/homePage.css';
import { GoLocation } from 'react-icons/go';
import { BiBus, BiCalendar } from 'react-icons/bi';
import UseHome from '../../logic/useHome';
import Bus from '../../atom/bus';
import loadingGif from '../../../assests/gif/eldoret-express.gif';
function HomePage({ isAdmin, setAlertMessage, setVariant }) {
	const {
		HandleSearch,
		SearchAnimation,
		BusCardAnimation,
		Buses,
		HandleDeparture,
		HandleArrival,
		HandleDate,
	} = UseHome();
	return (
		<div className="Home content">
			<div className="search" style={{ animation: SearchAnimation }}>
				<div className="search-items">
					<div className="search-items-box">
						<div className="search-item-icon">
							<GoLocation />
						</div>
						<div className="search-item-input">
							<input
								type="text"
								placeholder="Where to go"
								onChange={HandleArrival}
							></input>
						</div>
					</div>
				</div>
				<div className="search-items">
					<div className="search-items-box">
						<div className="search-item-icon">
							<BiBus />
						</div>
						<div className="search-item-input">
							<input
								type="text"
								placeholder="Departure from"
								onChange={HandleDeparture}
							></input>
						</div>
					</div>
				</div>
				<div className="search-items">
					<div className="search-items-box">
						<div className="search-item-icon">
							<BiCalendar />
						</div>
						<div className="search-item-input">
							<input
								type="date"
								placeholder="departure Date"
								onChange={HandleDate}
							></input>
						</div>
					</div>
				</div>
				<div className="search-items search-btn" onClick={HandleSearch}>
					Search
				</div>
			</div>
			<div id="loading-bus-gif">
				<img src={loadingGif} />
			</div>
			<div className="bus-cards" style={{ animation: BusCardAnimation }}>
				{Buses.map((data) => (
					<Bus data={data} key={data._id} isAdmin={isAdmin} />
				))}
			</div>
		</div>
	);
}
export default HomePage;
