const apiURL = 'http://localhost:5000/api';

const useApi = () =>{
    const apiCall = async({token, data, method, path }) => {
        // console.log(data,method,path,token);
        if(token){
            return fetch(`${apiURL}/${path}`, {
                method: method,
                headers: {
                    authorization: token,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(data => data.json());
        }else if(data){
            return fetch(`${apiURL}/${path}`, {
                method: method,
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(data => data.json());
        }
        else{
            return fetch(`${apiURL}/${path}`, {
                method: method,
                headers: {
                    'Content-Type': 'application/json'
                },
            })
                .then(data => data.json());
        }
    }
    return apiCall;
}
export default useApi;