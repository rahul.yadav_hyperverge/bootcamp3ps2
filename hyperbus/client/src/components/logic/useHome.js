import { useState } from 'react';
import useApi from './useApi';
const UseHome = () => {
	const apiCall = useApi();
	const [SearchAnimation, UseAnimation] = useState('');
	const [BusCardAnimation, UseBusCardAnimation] = useState('');
	const [Buses, UseBuses] = useState([]);
	const [departure, setDeparture] = useState();
	const [arrival, setArrival] = useState();
	const [date, setDate] = useState();
	const HandleDate = (e) => setDate(e.target.value);
	const HandleArrival = (e) => setArrival(e.target.value);
	const HandleDeparture = (e) => setDeparture(e.target.value);
	const HandleSearch = async () => {
		const data = { departure, arrival, date };
		const buses = await apiCall({
			method: 'POST',
			path: 'bus/getBuses',
			data: data,
		});
		UseBuses(buses);
		UseAnimation('search-move 1s ease-in-out forwards');
		UseBusCardAnimation('bus-card-display 2s ease-out forwards');
		document.getElementById('loading-bus-gif').style.display = 'none';
	};
	return {
		HandleSearch,
		SearchAnimation,
		BusCardAnimation,
		Buses,
		HandleDeparture,
		HandleArrival,
		HandleDate,
	};
};
export default UseHome;
