import { useState } from 'react';
import { Redirect } from 'react-router-dom';
import useApi from './useApi';

const UseAdmin = ({ setTokenState, setAlertMessage, setVariant }) => {
	const apiCall = useApi();
	const [email, setEmail] = useState();
	const [password, setPassword] = useState();
	const HandleEmail = (e) => setEmail(e.target.value);
	const HandlePassword = (e) => setPassword(e.target.value);
	const HandleLogIn = async () => {
		const data = { email, password };
		const { token, message, variant } = await apiCall({
			data: data,
			path: 'admin/login',
			method: 'POST',
		});
		setToken(token);
		setAlertMessage(message);
		setVariant(variant);
	};
	const HandleLogout = async () => {
		sessionStorage.removeItem('token');
		setTokenState('');
		setAlertMessage('Successfully Logged Out');
		setVariant('success');
		return <Redirect to ='/admin'/>
	};
	function setToken(userToken) {
		sessionStorage.setItem('token', JSON.stringify(userToken));
		setTokenState(userToken);
	}

	return { HandleLogIn, HandleEmail, HandlePassword, HandleLogout };
};
export default UseAdmin;
