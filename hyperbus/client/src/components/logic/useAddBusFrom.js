import { useState } from 'react';
import useApi from './useApi';
import getToken from './useToken';

const useAddBusFrom = ({ setAlertMessage, setVariant }) => {
	const apiCall = useApi();
	const [To, setTo] = useState();
	const [Date, setDate] = useState();
	const [From, setFrom] = useState();
	const [Price, setPrice] = useState();
	const [ArrivalTiming, setArrivalTiming] = useState();
	const [DepartureTiming, setDepartureTiming] = useState();
	const HandleTo = (e) => setTo(e.target.value);
	const HandleFrom = (e) => setFrom(e.target.value);
	const HandleDate = (e) => setDate(e.target.value);
	const HandlePrice = (e) => setPrice(e.target.value);
	const HandleArrivalTiming = (e) => setArrivalTiming(e.target.value);
	const HandleDepartureTiming = (e) => setDepartureTiming(e.target.value);
	const HandleAddBus = async () => {
		const data = {
			departure: From,
			arrival: To,
			departureDate: Date,
			departureTiming: DepartureTiming,
			arrivalTiming: ArrivalTiming,
			price: Price,
		};
		const token = getToken();
		const { message, variant} = await apiCall({
			data: data,
			path: 'bus/addbus',
			method: 'POST',
			token:token
		});
		setAlertMessage(message);
		setVariant(variant);
	};
	return {
		HandleAddBus,
		HandleArrivalTiming,
		HandleDate,
		HandleDepartureTiming,
		HandleFrom,
		HandlePrice,
		HandleTo,
	};
};
export default useAddBusFrom;
