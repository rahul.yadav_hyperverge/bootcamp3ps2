import { useState, useEffect } from 'react';
import useApi from '../logic/useApi';
function configSeats({ data, seatNumbers, UseSeatNumber }) {
	let new_seatNumbers = [];
	for (let i = 0; i < seatNumbers.length; i++) {
		let new_seatNumber = [seatNumbers[i][0]];
		let color = [];
		for (let j = seatNumbers[i][0]; j < seatNumbers[i][0] + 4; j++) {
			let found = 0;
			for (let z = 0; z < data.length; z++) {
				if (data[z] === j) {
					color.push('rgb(238, 164, 164)');
					found = 1;
				}
			}
			if (!found) {
				color.push('');
			}
		}
		new_seatNumber.push(color);
		new_seatNumbers.push(new_seatNumber);
	}
	UseSeatNumber(new_seatNumbers);
}
function configSeatDetails({ data, setSeatData }) {
	const bookedUserList = data.bookedUserList;
	let seatdetails={};
	for (const { seatNumber,userDetails } of bookedUserList) {
		seatdetails[seatNumber.toString()]=userDetails;
	}
	setSeatData(seatdetails);
}
const UseBooking = ({ busId, setAlertMessage, setVariant }) => {
	const apiCall = useApi();
	const [firstName, setFirstName] = useState();
	const [lastName, setLastName] = useState();
	const [age, setAge] = useState();
	const [phoneNumber, setPhoneNumber] = useState();
	const [userAge, setUserAge] = useState('Age');
	const [userFirstName, setUserFirstName] = useState('First Name');
	const [userLastName, setUserLastName] = useState('Last Name');
	const [userPhoneNumber, setUserPhoneNumber] = useState('Phone Number');
	const [busData, UseBusData] = useState('');
	const [seat, setSeat] = useState();
	const [seatData, setSeatData] = useState();
	const [seatNumbers, UseSeatNumber] = useState([
		[1, []],
		[5, []],
		[9, []],
		[13, []],
		[17, []],
		[21, []],
		[25, []],
		[29, []],
		[33, []],
		[37, []],
	]);
	const handleSeat = (number) => {
		setSeat(number);
		if(seatData[number]){
			console.log(
				'clicked'
			)
			document.getElementById('userDisplayForm').style.display='block';
			setUserFirstName(seatData[number].firstName);
			setUserLastName(seatData[number].lastName);
			setUserAge(seatData[number].age);
			setUserPhoneNumber(seatData[number].phoneNumber);
		}else{
			document.getElementById('userDisplayForm').style.display='none';
			setUserFirstName('Empty');
			setUserLastName('Empty');
			setUserPhoneNumber('Empty');
			setUserAge('Empty');
		}
	};
	const handleAge = (e) => setAge(e.target.value);
	const handlePhoneNumber = (e) => setPhoneNumber(e.target.value);
	const handleFirstName = (e) => setFirstName(e.target.value);
	const handleLastName = (e) => setLastName(e.target.value);
	const handleSubmit = async () => {
		let user = { age, firstName, lastName, phoneNumber };
		let data = { user, seat, busId };
		const { message, variant } = await apiCall({
			data: data,
			method: 'POST',
			path: `ticket/book/${busId}`,
		});
		setAlertMessage(message);
		setVariant(variant);
	};
	useEffect(async () => {
		const data = await apiCall({
			path: `bus/${busId}`,
			method: 'GET',
		});
		console.log(data);
		configSeats({ data: data.bookedSeats, seatNumbers, UseSeatNumber });
		configSeatDetails({ data, setSeatData });
		UseBusData(data);
	}, []);
	return {
		seat,
		busData,
		seatNumbers,
		handleAge,
		handleSeat,
		handleLastName,
		handleFirstName,
		handlePhoneNumber,
		handleSubmit,
		userFirstName,
		userLastName,
		userAge,
		userPhoneNumber,
	};
};
export default UseBooking;
