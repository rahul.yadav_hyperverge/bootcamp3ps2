import useApi from "./useApi";
import getToken from './useToken';
const UseBus = ({busId}) =>{
    const apiCall = useApi();
    const HandleCancelBus = async () =>{
        const token = getToken();
        const result = await apiCall({path:`bus/cancel/${busId}`,method:'POST',token:token});
    }
    return { HandleCancelBus };
}
export default UseBus;