import { useState, useEffect } from 'react';
function getToken() {
	const tokenString = sessionStorage.getItem('token');
	const userToken = JSON.parse(tokenString);
	return userToken;
}
const useApp = () => {
	const [tokenState, setTokenState] = useState();
	const [variant, setVariant] = useState();
	const [alertMessage, setAlertMessage] = useState();
	const [isAdmin, setIsAdmin] = useState(false);
	useEffect(() => {
		const token = getToken();
		setTokenState(token);
		setIsAdmin(tokenState?true:false);
	}, [tokenState]);
	return { isAdmin, tokenState, setTokenState,alertMessage,setAlertMessage,variant,setVariant };
};
export default useApp;
