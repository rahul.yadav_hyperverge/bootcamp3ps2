import { useState } from 'react';
import useApi from './useApi';
const UseTicket = ({setAlertMessage,setVariant}) => {
	const apiCall = useApi();
	const [SearchAnimation, UseAnimation] = useState('');
	const [TicketCardAnimation, UseTicketCardAnimation] = useState('');
	const [TicketNumber, UseTicketNumber] = useState('');
	const [TicketData, UseTicketData] = useState();
	const HandleSearch = async () => {
		const {data,message,variant} = await apiCall({
			method: 'GET',
			path: `ticket/get/${TicketNumber}`,
		});
		console.log(data);
		UseTicketData(data);
		if(variant == 'success'){
			UseAnimation('search-move 1s ease-in-out forwards');
			UseTicketCardAnimation('bus-card-display 2s ease-out forwards');
			document.getElementById('loadings-bus-gif').style.display='none';
		}
		// setAlertMessage(message);
		// setVariant(variant);
	};
	return {
		TicketData,
		HandleSearch,
		SearchAnimation,
		TicketCardAnimation,
		UseTicketNumber,
	};
};
export default UseTicket;
