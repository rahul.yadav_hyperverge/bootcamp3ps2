import '../../assests/styles/bus.css';
import { IconContext } from 'react-icons';
import { RiBus2Line } from 'react-icons/ri';
import { BiDotsHorizontalRounded } from 'react-icons/bi';
import moment from 'moment';
import { Link } from 'react-router-dom';
import UseBus from '../logic/useBus';
function Bus({ data, isAdmin }) {
	const { HandleCancelBus } = UseBus({busId:data._id});
	const formatedDate = moment(data.departureDate).format('DD-MM-YYYY');
	let busSeat = (
		<div className="bus-seats">
			<div className="bus-actual-price">₹200</div>
			<div className="bus-discounted-price">₹{data.price}</div>
			<Link to={`/booking/${data._id}`}>
				<div className="bus-book-btn">Book Now</div>
			</Link>
		</div>
	);
	if (isAdmin) {
		busSeat = (
			<div className="bus-seats">
				<div className="bus-cancel-btn" onClick={HandleCancelBus}>
					Cancel Bus
				</div>
				<Link to={`/booking/${data._id}`}>
					<div className="bus-book-btn">
						View Bus
					</div>
				</Link>
			</div>
		);
	}
	return (
		<div className="bus-box">
			<div className="bus-info">
				<div className="bus-schedule">
					<div className="bus-date">{formatedDate}</div>
					<div className="bus-time">
						<div className="bus-departure-time">
							<span>{data.departureTiming}</span>
						</div>
						<div className="bus-arrival-time">
							<span>{data.arrivalTiming}</span>
						</div>
					</div>
					<div className="bus-location">
						<div className="bus-departure-location">
							<span>{data.departure}</span>
						</div>
						<div className="bus-tras-logo">
							<IconContext.Provider value={{ color: '#bdbdbd' }}>
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<RiBus2Line />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
							</IconContext.Provider>
						</div>
						<div className="bus-arrival-location">
							<span>{data.arrival}</span>
						</div>
					</div>
					<div className="bus-seating-status">
						<div className="bus-total-seat">Total Seats - 40</div>
						<div className="bus-left-seat">
							Available Seats - {40 - data.bookedSeats.length}
						</div>
					</div>
				</div>
			</div>
			{busSeat}
		</div>
	);
}
export default Bus;
