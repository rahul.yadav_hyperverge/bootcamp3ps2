import '../../assests/styles/ticket.css';
import { IconContext } from 'react-icons';
import { RiBus2Line } from 'react-icons/ri';
import { BiDotsHorizontalRounded } from 'react-icons/bi';
import moment from 'moment';
function Ticket({data}) {
    if(!data){
        return <div></div>;
    }
	const ticketData = data.ticket;
	const busData = data.bus;
	const formatedDate = moment(busData.departureDate).format('DD-MM-YYYY');
	return (
		<div className="ticket-box">
			<div className="ticket-info">
				<div className="ticket-schedule">
					<div className="ticket-heading">
						<div className="ticket-username">{`${ticketData.userDetails.firstName} ${ticketData.userDetails.lastName}`}</div>
						{/* <div className="ticket-cancel-btn" >Cancel</div> */}
					</div>
					<div className="ticket-time">
						<div className="ticket-departure-time">
							<span>{busData.departureTiming}</span>
						</div>
						<div className="ticket-arrival-time">
							<span>{busData.arrivalTiming}</span>
						</div>
					</div>
					<div className="ticket-location">
						<div className="ticket-departure-location">
							<span>{busData.departure}</span>
						</div>
						<div className="ticket-tras-logo">
							<IconContext.Provider value={{ color: '#bdbdbd' }}>
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<RiBus2Line />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
								<BiDotsHorizontalRounded />
							</IconContext.Provider>
						</div>
						<div className="ticket-arrival-location">
							<span>{busData.arrival}</span>
						</div>
					</div>
				</div>
			</div>
			<div className="ticket-seats">
				<div className="ticket-seat-no">
                    {ticketData.seatNumber}
                </div>
                <div style={{fontSize:'30px',padding:'10px'}}>Seat No.</div>
			</div>
		</div>
	);
}
export default Ticket;
