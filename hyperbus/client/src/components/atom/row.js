import '../../assests/styles/row.css';
import Seat from './seat';
function Row({ seatNumber,handleSeat }) {
	const seatnumbers = [];
	for (let i = seatNumber[0]; i <= seatNumber[0] + 3; i++) {
		seatnumbers.push(i);
	}
	return (
		<div className="bus-seat-row">
			<Seat number={seatnumbers[0]} color={seatNumber[1][0]} handleSeat={handleSeat}/>
			<Seat number={seatnumbers[1]} color={seatNumber[1][1]} handleSeat={handleSeat}/>
			<div style={{ width: '70px' }}></div>
			<Seat number={seatnumbers[2]} color={seatNumber[1][2]} handleSeat={handleSeat}/>
			<Seat number={seatnumbers[3]} color={seatNumber[1][3]} handleSeat={handleSeat}/>
		</div>
	);
}
export default Row;
