import { useState } from 'react';
import '../../assests/styles/seat.css';

function Seat({number, color, handleSeat}) {
	const [colorBtn , UseColorBtn] = useState(color);
	let textColor = 'white';
	const handleClick = () =>{
		UseColorBtn('rgb(175, 230, 94)');
		handleSeat(number);
	}
	if(!color){
		textColor='rgb(190, 190, 190)';
	}
	return (
		<div className="seat">
			<div className="seat-box" style={{backgroundColor:`${color}`,color:textColor}} onClick={handleClick}>{number}</div>
		</div>
	);
}
export default Seat;
