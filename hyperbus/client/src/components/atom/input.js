import '../../assests/styles/input.css';

function Input(props){
    return (
        <div className="input-box" style={{width:props.width}}>
            <input type={props.type} placeholder={props.placeholder} onChange={props.onChange}/>
        </div>
    );
}
export default Input;