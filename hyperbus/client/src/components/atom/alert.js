import 'bootstrap/dist/css/bootstrap.min.css';
import { Alert } from 'react-bootstrap';
import { useEffect, useState } from 'react';
function AlertDismissible({ variant, message }) {
	const [show, setShow] = useState(false);
	useEffect(()=>{
		setShow(true);
	},[message]);

	if (show && message) {
		return (
			<Alert style={{marginBottom:'0px'}} variant={variant} onClose={() => setShow(false)} dismissible>
				{message}
			</Alert>
		);
	}
	return <div></div>;
}
export default AlertDismissible;
