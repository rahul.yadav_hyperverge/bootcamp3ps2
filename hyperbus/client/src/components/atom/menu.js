import '../../assests/styles/menu.css';
import { Link, withRouter } from 'react-router-dom';
import { useState, useEffect } from 'react';

function Menu({ isAdmin }) {
	const currentPath = window.location.pathname;
	let [menuState, setMenuState] = useState({});
	useEffect(() => {
		if (currentPath === '/status')
			setMenuState({ 1: false, 2: true, 3: false });
		else if (currentPath === '/admin')
			setMenuState({ 1: false, 2: false, 3: true });
		else 
			setMenuState({ 1: true, 2: false, 3: false });
	}, []);
	const handleBus = () => setMenuState({ 1: true, 2: false, 3: false });
	const handleTicket = () => setMenuState({ 1: false, 2: true, 3: false });
	const handleAdmin = () => setMenuState({ 1: false, 2: false, 3: true });
	let menuBus = menuState[1] ? 'item item-selected' : 'item';
	let menuTicket = menuState[2] ? 'item item-selected' : 'item';
	let menuAdmin = menuState[3] ? 'item item-selected' : 'item';
	let showAdmin;
	if (isAdmin) {
		showAdmin = (
			<Link to="/admin">
				<li className={menuAdmin} onClick={handleAdmin}>
					Admin Panel
				</li>
			</Link>
		);
	}
	return (
		<div className="Menu">
			<div className="Menu-box">
				<div className="Menu-logo">ROAD...TIME</div>
				<div className="Menu-items">
					<ul className="Menu-items-list">
						<Link to="/">
							<li className={menuBus} onClick={handleBus}>
								Bus
							</li>
						</Link>
						<Link to="/status">
							<li className={menuTicket} onClick={handleTicket}>
								Ticket Status
							</li>
						</Link>
						{showAdmin}
					</ul>
				</div>
				<div className="site-contact">
					<div className="site-contact-items">Help</div>
					<div className="site-contact-items">Support</div>
					<div className="site-contact-items">About Us</div>
					<Link to="/admin">
						<div className="site-contact-items">Admin</div>
					</Link>
				</div>
			</div>
		</div>
	);
}
export default withRouter(Menu);
