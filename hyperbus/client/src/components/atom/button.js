import '../../assests/styles/button.css';

function Button(props) {
	return (
		<div
			className="button-box"
			style={{ width: props.width }}
			onClick={props.onClick}
		>
			{props.text}
		</div>
	);
}
export default Button;
