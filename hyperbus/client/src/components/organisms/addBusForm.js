import '../../assests/styles/addBusForm.css';
import Input from '../atom/input';
import busGif from '../../assests/gif/bus-animation-1.gif';
import useAddBusFrom from '../logic/useAddBusFrom';
function AddBusForm({ setAlertMessage, setVariant }) {
	const {
		HandleAddBus,
		HandleArrivalTiming,
		HandleDate,
		HandleDepartureTiming,
		HandleFrom,
		HandlePrice,
		HandleTo,
	} = useAddBusFrom({setAlertMessage, setVariant });
	return (
		<div className="form-box">
			<div className="form-text">
				<span className="bus-form-heading">Add Bus</span>
				<div className="add-bus-btn" onClick={HandleAddBus}>
					Add
				</div>
			</div>
			<div className="bus-form">
				<div className="bus-form-details">
					<div className="location-entry">
						<Input placeholder="From" width="100%" onChange={HandleFrom} />
						<Input placeholder="To" width="100%" onChange={HandleTo} />
					</div>
					<Input
						placeholder="Date"
						type="date"
						width="100%"
						onChange={HandleDate}
					/>
					<Input
						placeholder="Departure Timing"
						width="100%"
						onChange={HandleDepartureTiming}
					/>
					<Input
						placeholder="Arrival Timing"
						width="100%"
						onChange={HandleArrivalTiming}
					/>
					<Input placeholder="Price" width="100%" onChange={HandlePrice} />
				</div>
				<div className="bus-gif">
					<img src={busGif} />
				</div>
			</div>
		</div>
	);
}
export default AddBusForm;
