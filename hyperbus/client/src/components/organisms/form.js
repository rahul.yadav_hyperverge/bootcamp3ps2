import '../../assests/styles/form.css';
import Input from '../atom/input';
import Button from '../atom/button';

function Form({
	handleSubmit,
	handleAge,
	handleFirstName,
	handleLastName,
	handlePhoneNumber,
	seat,
}) {
	return (
		<div className="form">
			<div className="form-heading">Fill the details</div>
			<div className="form-fill">
				<div className="form-details">
					<div className="form-name">
						<Input
							placeholder="First name"
							width="50%"
							onChange={handleFirstName}
						/>
						<Input
							placeholder="Last name"
							width="50%"
							onChange={handleLastName}
						/>
					</div>
					<Input
						placeholder="Phone number"
						width="100%"
						onChange={handlePhoneNumber}
					/>
					<Input placeholder="Age" width="100%" onChange={handleAge} />
					<Button width="95%" text="Book Seat" onClick={handleSubmit} />
				</div>
				<div className="form-seat-no">
					<div className="form-seat-no-box">
						<div className="seat-no">{seat}</div>
						<div className="form-seat">Seat no.</div>
					</div>
				</div>
			</div>
		</div>
	);
}
export default Form;
