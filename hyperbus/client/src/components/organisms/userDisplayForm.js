import '../../assests/styles/form.css';
import Input from '../atom/input';

function UserDisplayForm({
	userAge,
	userFirstName,
	userLastName,
	userPhoneNumber,
	userSeat,
}) {
	return (
		<div class="form" id="userDisplayForm" style={{display:'none'}}>
			<div className="form-heading">Ticket details</div>
			<div className="form-fill">
				<div className="form-details">
					<div className="form-name">
						<Input
							placeholder={userFirstName}
							width="50%"
							readonly="readonly"
						/>
						<Input
							placeholder={userLastName}
							width="50%"
							readonly="readonly"
						/>
					</div>
					<Input
						placeholder={userPhoneNumber}
						width="100%"
						readonly="readonly"
					/>
					<Input
						placeholder={userAge}
						width="100%"
						readonly="readonly"
					/>
				</div>
				<div className="form-seat-no">
					<div className="form-seat-no-box">
						<div className="seat-no">{userSeat}</div>
						<div className="form-seat">Seat no.</div>
					</div>
				</div>
			</div>
		</div>
	);
}
export default UserDisplayForm;
