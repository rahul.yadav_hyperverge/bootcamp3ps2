const router = require('express').Router();
const {
	getBuses,
	addBus,
	getBus,
	cancelBus,
} = require('../controllers/bus-controller');
const verify = require('../middlewares/verifyToken');
router.post('/getBuses', async (req, res) => {
	try {
		const buses = await getBuses(req.body);
		return res.send(buses);
	} catch (e) {
		console.error("Can't get all buses", e.message);
		return res.status(400).send("can't get all buses");
	}
});
router.post('/addBus', verify, async (req, res) => {
	try {
		const result = await addBus(req.body, res);
		return result;
	} catch (e) {
		console.error('Bus not added', e.message);
		return res.status(400).send({
			message: 'Bus is Not added',
			variant: 'danger',
		});
	}
});
router.get('/:id', async (req, res) => {
	try {
		const result = await getBus(req.params.id);
		return res.send(result);
	} catch (e) {
		console.log("can't get the bus details", e.message);
		return res.status(400).send("can't send the bus data");
	}
});
router.post('/cancel/:id', verify, async (req, res) => {
	try {
		const message = await cancelBus(req.params.id);
		return res.send({
			message: message,
			variant: 'success',
		});
	} catch (e) {
		console.error('Bus is not deleted', e.message);
		return res.status(400).send({
			messsage: 'Bus is not deleted',
			variant: 'danger',
		});
	}
});

module.exports = router;
