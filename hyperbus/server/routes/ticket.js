const router = require('express').Router();
const {
	bookTicket,
	cancelTicket,
	getTicket,
} = require('../controllers/ticket-controller');

router.post('/book/:id', async (req, res) => {
	try {
		const busId = req.params.id;
		const result = await bookTicket({data:req.body, res, busId});
		return res
			.send({
				message:'Your Ticket is booked',
				variant:'success'
			});
	} catch (e) {
		console.error('Seat is not booked', e.message);
		return res
			.status(400)
			.send({
				message:'Your Ticket is Not booked',
				variant:'danger'
			});
	}
});
router.post('/cancel/:id',async (req,res) =>{
	try{
		const ticketId = req.params.id;
		const result = await cancelTicket(ticketId);
		return res.send(result);
	}
	catch (e) {
		console.error('Seat is not cancelled', e.message);
		return res.status(400).send('seat is not cancelled');
	}
})
router.get('/get/:id', async (req, res) => {
	try {
		const ticketId = req.params.id;
		const { ticket, bus } = await getTicket(ticketId);
		const result = { ticket, bus };
		return res
			.send({
				data:result,
				message:'Ticket Found',
				variant:'success'
			});
	} catch (e) {
		console.error('Ticket no found', e.message);
		return res
			.status(400)
			.send({
				message:'No ticket with this Id',
				variant:'danger'
			});
	}
});

module.exports = router;
