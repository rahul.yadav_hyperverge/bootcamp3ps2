const router = require('express').Router();
const { adminLogin, adminSignup } = require('../controllers/admin-controller');
router.post('/login', async (req, res) => {
	try {
		const token = await adminLogin(req.body, res);
		return res.send({
			token,
			message: 'Successfully Logged In',
			variant: 'success',
		});
	} catch (e) {
		console.error("can't login to admin", e.message);
		return res.status(400).send({ 
			message: "Can't Log in", 
			variant:'danger',
			token: '' 
		});
	}
});
router.post('/signup', async (req, res) => {
	try {
		const token = await adminSignup(req.body, res);
		return res.send({
			token,
			message: 'Successfull Sign Up',
			variant: 'success',
		});
	} catch (e) {
		console.error("can't login to admin", e.message);
		return res.status(400).send({
			message: "Can't Sign up",
			variant:'danger',
			token: '' 
		});
	}
});

module.exports = router;
