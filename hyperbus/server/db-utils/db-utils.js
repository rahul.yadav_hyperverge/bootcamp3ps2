const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { Bus } = require('../models/bus');
const { User } = require('../models/user');
const { Admin } = require('../models/admin');
const { Ticket } = require('../models/ticket');

//admin-controller
async function findAdminByEmail({ email }) {
	return await Admin.findOne({ email: email });
}

async function validHashPassword({ password, user }) {
	return await bcrypt.compare(password, user.password);
}

async function generateToken(user) {
	return jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
}

async function generateHashPassword({ password }) {
	const salt = await bcrypt.genSalt(10);
	return await bcrypt.hash(password, salt);
}
async function newAdmin({ name, email, hashPassword }) {
	adminUser = new Admin({
		name: name,
		email: email,
		password: hashPassword,
	});
	const adminSaved = await adminUser.save();
	return adminUser;
}

//bus-controller
async function findBuses() {
	return await Bus.find().limit(20);
}
async function findBusesByArrival({ arrival }) {
	return await Bus.find({ arrival: arrival });
}
async function findBusesByDeparture({ departure }) {
	return await Bus.find({ departure: departure });
}
async function findBusesByArrivalDeparture({ arrival, departure }) {
	return await Bus.find({ arrival: arrival, departure: departure });
}
async function findBusById({busId,selectItems}) {
	return await Bus.findById(busId).select(selectItems)
		.populate({
			path: 'bookedUserList',
			populate:{
				path:'userDetails',
				model:'User'
			}
		}).exec();
}
async function newBus({
	departure,
	arrival,
	departureDate,
	departureTiming,
	arrivalTiming,
	price,
}) {
	const bus = new Bus({
		departure: departure,
		arrival: arrival,
		departureDate: departureDate,
		departureTiming: departureTiming,
		arrivalTiming: arrivalTiming,
		price: price,
	});
	const savedBus = await bus.save();
	return 'New Bus is Added';
}
async function deleteBusbyId(busId) {
	return await Bus.deleteOne({ _id: busId });
}

//ticket controller
async function newUser({ firstName, lastName, phoneNumber, age }) {
	const user = new User({
		firstName: firstName,
		lastName: lastName,
		phoneNumber: phoneNumber,
		age: age,
	});
	const savedUser = await user.save();
	return user;
}
async function newTicket({ seatNumber, userDetails, busId }) {
	const ticket = new Ticket({
		seatNumber: seatNumber,
		userDetails: userDetails,
		busId: busId,
	});
	const savedTicket = await ticket.save();
	return ticket;
}
async function updateBus({ bus, ticket, seat }) {
	bus.bookedUserList.push(ticket);
	bus.bookedSeats.push(seat);
	const updatedBus = await bus.save();
	return updatedBus;
}
async function findTicketById(ticketId) {
	return await Ticket.findById(ticketId);
}
async function findTicketByIdAndUpdate({ ticket, values, key }) {
	return await Bus.findByIdAndUpdate(ticket.busId, {
		$pull: { key: [values] },
	});
}
async function deleteUser(userId) {
	return await User.deleteOne({ _id: userId });
}
async function deleteTicket(ticketId) {
	return await Ticket.deleteOne({ _id: ticketId });
}
async function findTicketByIdAndPopulate({ticketId,key}){
	return await Ticket.findById(ticketId).populate(key);
}

module.exports.newBus = newBus;
module.exports.newUser = newUser;
module.exports.newAdmin = newAdmin;
module.exports.newTicket = newTicket;
module.exports.updateBus = updateBus;
module.exports.findBuses = findBuses;
module.exports.deleteUser = deleteUser;
module.exports.findBusById = findBusById;
module.exports.deleteTicket = deleteTicket;
module.exports.generateToken = generateToken;
module.exports.deleteBusbyId = deleteBusbyId;
module.exports.findTicketById = findTicketById;
module.exports.findAdminByEmail = findAdminByEmail;
module.exports.validHashPassword = validHashPassword;
module.exports.findBusesByArrival = findBusesByArrival;
module.exports.findBusesByDeparture = findBusesByDeparture;
module.exports.generateHashPassword = generateHashPassword;
module.exports.findTicketByIdAndUpdate = findTicketByIdAndUpdate;
module.exports.findTicketByIdAndPopulate = findTicketByIdAndPopulate;
module.exports.findBusesByArrivalDeparture = findBusesByArrivalDeparture;
