const { ticketValidation } = require('../models/ticket');
const { userValidation } = require('../models/user');
const {
	newUser,
	newTicket,
	findBusById,
	updateBus,
	findTicketById,
	findTicketByIdAndUpdate,
	findTicketByIdAndPopulate,
} = require('../db-utils/db-utils');
async function bookTicket({ data, res, busId }) {
	const userData = data.user;

	const { error } = userValidation(userData);
	if (error)
		return res.status(400).send({
			message: 'Invalid Ticket Details',
			variant: 'warning',
		});

	const user = await newUser({
		firstName: userData.firstName,
		lastName: userData.lastName,
		phoneNumber: userData.phoneNumber,
		age: userData.age,
	});
	const seatData = { seatNumber: data.seat };
	const ticketError = ticketValidation(seatData);
	if (ticketError.error)
		return res.stats(400).send({
			message: 'Invalid Ticket Details',
			variant: 'warning',
		});

	const ticket = await newTicket({
		seatNumber: data.seat,
		userDetails: user,
		busId: busId,
	});
	const bus = await findBusById({busId,selectItems:[]});
	const updatedBus = await updateBus({
		bus: bus,
		ticket: ticket,
		seat: data.seat,
	});
	return updatedBus;
}
async function cancelTicket(ticketId) {
	// const ticket = await Ticket.findById(ticketId);
	const ticket = await findTicketById(ticketId);
	// const updatedBookedSeat = await Bus.findByIdAndUpdate(ticket.busId, {
	// 	$pull: { bookedSeats: [ticket.seatNumber.toString()] },
	// });
	const updatedBookedSeat = await findTicketByIdAndUpdate({
		ticket,
		key: 'bookedSeats',
		values: ticket.seatNumber.toString(),
	});
	// const updatedBookedUserList = await Bus.findByIdAndUpdate(ticket.busId, {
	// 	$pull: { bookedUserList: [ticket] },
	// });
	const updatedBookedUserList = await findTicketByIdAndUpdate({
		ticket,
		key: 'bookedUserList',
		values: ticket,
	});
	const userDeleted = await deleteUser(ticket.userDetails);
	const deleteTicket = await deleteTicket(ticketId);
	return 'ticket is cancelled';
}
async function getTicket(ticketId) {
	const ticket = await findTicketByIdAndPopulate({ticketId,key:'userDetails'});
	const bus = await findBusById({busId:ticket.busId,selectItems:['-bookedUserList']});
	return { ticket, bus };
}

module.exports.getTicket = getTicket;
module.exports.bookTicket = bookTicket;
module.exports.cancelTicket = cancelTicket;
