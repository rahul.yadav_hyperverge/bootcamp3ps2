const {
	loginValidation,
	registrationValidation,
} = require('../models/admin');
const {
	findAdminByEmail,
	validHashPassword,
	generateToken,
	generateHashPassword,
	newAdmin,
} = require('../db-utils/db-utils');

async function adminLogin(body, res) {
	const { error } = loginValidation(body);
	if (error)
		return res.status(400).send({
			message: 'Invalid Credentials',
			variant: 'warning',
			token: '',
		});

	const user = await findAdminByEmail({ email: body.email });
	if (!user)
		return res.status(400).send({
			message: 'Invalid Email or Password',
			variant: 'warning',
			token: '',
		});

	const validPassword = await validHashPassword({
		password: body.password,
		user,
	});
	if (!validPassword)
		return res.status(400).send({
			message: 'Invalid Email or Password',
			variant: 'warning',
			token: '',
		});

	const token = await generateToken(user);
	return token;
}
async function adminSignup(body, res) {
	const { error } = registrationValidation(body);
	if (error)
		return res.status(400).send({
			message: 'Invalid Credentials',
			variant: 'warning',
		});

	let user = await findAdminByEmail({ email: body.email });
	if (user)
		return res.status(400).send({
			message: 'User Already Registered',
			variant: 'warning',
			token: '',
		});

	const hashPassword = await generateHashPassword({ password: body.password });
	const adminUser = await newAdmin({
		name: body.name,
		email: body,
		email,
		hashPassword,
	});
	const token = await generateToken(adminUser);
	return token;
}

module.exports.adminLogin = adminLogin;
module.exports.adminSignup = adminSignup;
