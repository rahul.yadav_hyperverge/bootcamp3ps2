const { busValidation } = require('../models/bus');
const {
	findBuses,
	findBusesByArrivalDeparture,
	findBusesByDeparture,
	findBusesByArrival,
	newBus,
	findBusById,
	deleteBusbyId,
} = require('../db-utils/db-utils');
async function getBuses(data) {
	let buses;
	buses = await findBuses();
	if (data.arrival && data.departure)
		buses = await findBusesByArrivalDeparture({
			arrival: data.arrival,
			departure: data.departure,
		});
	else if (data.arrival)
		buses = await findBusesByArrival({
			arrival: data.arrival,
		});
	else if (data.departure)
		buses = await findBusesByDeparture({
			departure: data.departure,
		});
	return buses;
}
async function addBus(data, res) {
	const { error } = busValidation(data);
	if (error)
		return res.status(400).send({
			message: 'Invalid Bus Data',
			variant: 'warning',
		});
	const result = await newBus({
		departure: data.departure,
		arrival: data.arrival,
		departureDate: new Date(data.departureDate),
		departureTiming: data.departureTiming,
		arrivalTiming: data.arrivalTiming,
		price: data.price,
	});
	return res.send({
		message: result,
		variant: 'success',
	});
}
async function getBus(busId) {
	const bus = await findBusById({ busId, selectItems: [] })
	return bus;
}
async function cancelBus(busId) {
	const deleteBus = await deleteBusbyId(busId);
	return 'Bus is Successfully Cancelled';
}

module.exports.addBus = addBus;
module.exports.getBus = getBus;
module.exports.getBuses = getBuses;
module.exports.cancelBus = cancelBus;
