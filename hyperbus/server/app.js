const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');

dotenv.config();

//Routes
const busRouter = require('./routes/bus');
const ticketRouter = require('./routes/ticket');
const adminRouter = require('./routes/admin');
//Connect to DB
mongoose.connect(
	process.env.DB_CONNECT,
	{ useNewUrlParser: true, useUnifiedTopology: true },
	() => console.log('Db is now connected...')
);

//Middleware
app.use(express.json());
app.use(cors());

//Routes Middleware
app.use('/api/bus', busRouter);
app.use('/api/ticket/', ticketRouter);
app.use('/api/admin',adminRouter);

const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`server listening to port ${port}...`));
