const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
const Bus = mongoose.model(
	'bus',
	new mongoose.Schema({
		departure: {
			type: String,
            required: true,
            maxlength:255
		},
		arrival: {
			type: String,
            required: true,
            maxlength:255
		},
		departureDate: {
			type: Date,
			required: true,
		},
		departureTiming: {
			type: String,
			required: true,
		},
		arrivalTiming: {
			type: String,
			required: true,
		},
		price: {
			type: Number,
            required: true,
            min:0
		},
		bookedSeats: {
			type: Array,
			default: [],
		},
		bookedUserList: [{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Ticket',
		}],
	})
);

function busValidation(body) {
	const schema = Joi.object({
        departure: Joi.string().max(255).required(),
        arrival: Joi.string().max(255).required(),
        departureDate: Joi.date().required(),
        departureTiming: Joi.string().required(),
        arrivalTiming: Joi.string().required(),
        price: Joi.number().min(0).required(),
    });
	return schema.validate(body);
}
module.exports.Bus = Bus;
module.exports.busValidation = busValidation;
