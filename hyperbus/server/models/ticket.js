const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

const Ticket = mongoose.model(
	'Ticket',
	new mongoose.Schema({
		seatNumber: {
			type: Number,
			required: true,
		},
		userDetails: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User',
			required: true,
        },
        busId:{
            type:String,
            required:true,
        }
	})
);
function ticketValidation(body) {
	const schema = Joi.object({
		seatNumber: Joi.number().required(),
	});
	return schema.validate(body);
}
module.exports.Ticket = Ticket;
module.exports.ticketValidation = ticketValidation;
