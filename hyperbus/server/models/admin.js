const Joi = require('@hapi/joi');
const mongoose= require('mongoose');

const Admin = mongoose.model('Admin',
    new mongoose.Schema({
        name: {
            type: String,
            required: true,
            maxlength: 255
        },
        email: {
            type: String,
            required: true,
            minlength: 6,
            maxlength: 255,
            unique: true
        },
        password: {
            type: String,
            required: true,
            minlength: 6,
            maxlength: 1024
        }
    })
);

function registrationValidation(user){
    const schema =  Joi.object({
        name: Joi.string().required().min(3).max(255).required(),
        email: Joi.string().email().required(),
        password: Joi.string().required().min(6).max(255).required()
    });
    return schema.validate(user);
};
function loginValidation(user){
    const schema = Joi.object({
        email: Joi.string().email().max(255).required(),
        password: Joi.string().min(6).max(255).required()
    })
    return schema.validate(user);
}


module.exports.Admin = Admin;
module.exports.loginValidation = loginValidation;
module.exports.registrationValidation = registrationValidation;

