const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

const User = mongoose.model(
	'User',
	new mongoose.Schema({
		firstName: {
			type: String,
            required: true,
            maxlength:255
		},
		lastName: {
			type: String,
            required: true,
            maxlength:255
		},
		phoneNumber: {
			type: Number,
            required: true,
            maxlength:10,
            minlength:10
		},
		age: {
			type: Number,
			required: true,
		},
	})
);

function userValidation(body) {
	const schema = Joi.object({
		firstName: Joi.string().max(255).required(),
		lastName: Joi.string().max(255).required(),
		phoneNumber: Joi.number().required(),
		age: Joi.number().required(),
	});
	return schema.validate(body);
}
module.exports.User = User;
module.exports.userValidation = userValidation;
